﻿/*===================================================================================*/
/*	ANGULARJS : SERVICE - VISITEURS
/*===================================================================================*/

app.service("VisiteursSvc", function ($http) {
    var self = this;
    self.visiteurs = [];

    self.promise = $http.get("/api/visiteur.php").success(function (response) {
        self.visiteurs = response;
    });

    self.get = function (idVisiteur) {
        for (var i = 0; i < self.visiteurs.length; i++) {
            if (self.visiteurs[i].Id == idVisiteur)
                return self.visiteurs[i];
        }

        return null;
    };
});

/*===================================================================================*/
/*	ANGULARJS : SERVICE - MEDOCS
/*===================================================================================*/

app.service("MedocsSvc", function ($http) {
    var self = this;
    self.medocs = [];

    self.promise = $http.get("/api/medicament.php").success(function (response) {
        self.medocs = response;
    });

    self.update = function (medoc) {
        $http.post("/api/medicament.php", medoc).success(function (response) {
            if (!response)
                alert("Erreur lors de la mise à jour du prix.");
        });
    }

    self.get = function (idMedoc) {
        for (var i = 0; i < self.medocs.length; i++) {
            if (self.medocs[i].Id == idMedoc)
                return self.medocs[i];
        }

        return null;
    };
});

/*===================================================================================*/
/*	ANGULARJS : SERVICE - SECTEURS
/*===================================================================================*/

app.service("SecteursSvc", function ($http) {
    var self = this;
    self.secteurs = [];

    self.promise = $http.get("/api/secteur.php").success(function (response) {
        self.secteurs = response;
    });

    self.insert = function (assignation) {
        $http.put("/api/secteur.php", assignation).success(function (response) {
            if (!response)
                alert("Erreur lors de l'assignation.");
        });
    }

    self.delete = function (assignation) {
        $http.post("/api/secteur.php", assignation).success(function (response) {
            if (!response)
                alert("Erreur lors de la suppression.");
        });
    }
});

/*===================================================================================*/
/*	ANGULARJS : SERVICE - PRATICIENS
/*===================================================================================*/

app.service('PraticiensSvc', function ($http) {
    var self = this;
    self.praticiens = [];

    self.promise = $http.get("/api/praticien.php").success(function (response) {
        self.praticiens = response;
    });

    self.get = function (idPraticien) {
        for (var i = 0; i < self.praticiens.length; i++) {
            if (self.praticiens[i].Id == idPraticien)
                return self.praticiens[i];
        }

        return null;
    };
});


/*===================================================================================*/
/*	ANGULARJS : SERVICE - USER
/*===================================================================================*/

app.service("UsersSvc", function ($http, $location) {
    var self = this;
    self.user = JSON.parse(window.localStorage.getItem("user"));

    self.deconnexion = function () {
        window.localStorage.removeItem("user");
        self.user = null;
        $location.url("/login");
    }

    self.checkConnected = function () {
        if (!self.user || self.user == null) {
            $location.url("/login");
            return false;
        } else {
            return self.user;
        }
    }
});


/*===================================================================================*/
/*	ANGULARJS : SERVICE - COMMANDES
/*===================================================================================*/

app.service("CommandesSvc", function () {
    var self = this;

    self.get = function (praticien, idCommande) {
        for (var i = 0; i < praticien.Commandes.length; i++) {
            if (praticien.Commandes[i].Id == idCommande)
                return praticien.Commandes[i];
        }

        return null;
    };
});