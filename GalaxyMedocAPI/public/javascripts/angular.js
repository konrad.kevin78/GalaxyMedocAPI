/*===================================================================================*/
/*	ANGULARJS : GALAXYMEDOC
/*===================================================================================*/

var app = angular.module("galaxymedoc", ["ngCookies", "ngRoute", "ngResource"]);

/*===================================================================================*/
/*	ANGULARJS : ROUTER
/*===================================================================================*/

app.config(function ($routeProvider, $locationProvider) {
    $routeProvider

        /* ===== ANGULARJS : ROUTER - LOGIN ===== */
        .when("/", {
            templateUrl: "/views/pages/login.html",
            controllerAs: "login",
            resolve: {
                user: function (UsersSvc) { return UsersSvc.user; },
            },
            controller: function ($scope, $http, $location, UsersSvc) {
                var self = this;
                self.loggedUser = UsersSvc.user;
                self.erreur = false;
                self.passeEnvoye = false;
                self.user = {
                    remember: true
                };

                if (self.loggedUser)
                    window.location.href = "/#/historique";

                self.connexion = function () {
                    self.erreur = false;

                    $http.post("/api/user.php?service=connexion", self.user).success(function (response) {
                        if (response) {
                            UsersSvc.user = response;

                            if (self.user.remember)
                                window.localStorage.setItem("user", JSON.stringify(UsersSvc.user));

                            self.user = {};
                            window.location.href = "/#/historique";
                        }
                        else
                            self.erreur = true;
                    });
                }

                self.recover = function () {
                    self.passeEnvoye = false;
                    $http.post("/api/user.php?service=recover", self.user).success(function (response) {
                        self.passeEnvoye = true;
                    });
                }
            }
        })

        /* ===== ANGULARJS : ROUTER - HOME ===== */
        .when("/historique", {
            templateUrl: "/views/pages/historique.html",
            controllerAs: "home",
            controller: function ($scope, $location, UsersSvc, PraticiensSvc) {
                var self = this;

                UsersSvc.checkConnected();
                self.praticiens = PraticiensSvc;

                self.charge = function (praticien) {
                    $location.url("historique/"+praticien.Id);
                }

                $scope.search = function (praticien) {
                    if (!$scope.recherche || (praticien.Nom.toLowerCase().indexOf($scope.recherche.toLowerCase()) != -1) || 
                        (praticien.Prenom.toLowerCase().indexOf($scope.recherche.toLowerCase()) != -1) || 
                        (praticien.Secteur.Region.Nom.toLowerCase().indexOf($scope.recherche.toLowerCase()) != -1) || 
                        (praticien.Secteur.Nom.toLowerCase().indexOf($scope.recherche.toLowerCase()) != -1)) {
                        return true;
                    }
                    return false;
                };
            }
        })

        /* ===== ANGULARJS : ROUTER - HISTORIQUE ===== */
        .when("/historique/:idPraticien", {
            templateUrl: "/views/pages/praticien.html",
            controllerAs: "historique",
            resolve: {
                user: function (UsersSvc) { return UsersSvc.checkConnected(); },
                praticiens: function (PraticiensSvc) { return PraticiensSvc.promise; }
            },
            controller: function ($location, $routeParams, PraticiensSvc) {
                var self = this;
                self.praticien = PraticiensSvc.get($routeParams.idPraticien);

                self.detailCommande = function (commande) {
                    $location.url("historique/" + self.praticien.Id + "/" + commande.Id);
                }
            }
        })

        /* ===== ANGULARJS : ROUTER - DETAILCOMMANDE ===== */
        .when("/historique/:idPraticien/:idCommande", {
            templateUrl: "/views/pages/detailCommande.html",
            controllerAs: "detailCom",
            resolve: {
                user: function (UsersSvc) { return UsersSvc.checkConnected(); },
                visiteurs: function (VisiteursSvc) { return VisiteursSvc.promise; },
                praticiens: function (PraticiensSvc) { return PraticiensSvc.promise; }
            },
            controller: function (PraticiensSvc, CommandesSvc, VisiteursSvc, $routeParams) {
                var self = this;
                self.praticien = PraticiensSvc.get($routeParams.idPraticien);
                self.commande = CommandesSvc.get(self.praticien, $routeParams.idCommande);
                self.commande.visiteur = VisiteursSvc.get(self.commande.IdVisiteur);
            }
        })

        /* ===== ANGULARJS : ROUTER - MEDOCS ===== */
        .when("/medicaments", {
            templateUrl: "/views/pages/medicaments.html",
            controllerAs: "medicaments",
            resolve: {
                user: function (UsersSvc) { return UsersSvc.checkConnected(); },
                medocs: function (MedocsSvc) { return MedocsSvc.promise; }
            },
            controller: function (MedocsSvc) {
                var self = this;
                self.erreur = false;
                self.medocs = MedocsSvc.medocs;
                // TODO : Fond rouge si valeur non valide (text par exemple, ou si possible, effacer la saisie si textuelle)
                self.updatePrix = function (medoc, valeur) {
                    medoc.Prix = parseFloat(medoc.Prix) + valeur;
                }

                self.update = function (medoc) {
                    medoc.Prix = parseFloat(medoc.Prix);
                    MedocsSvc.update(medoc);
                }
            }
        })

        /* ===== ANGULARJS : ROUTER - ASSIGNATION ===== */
        .when("/assignation", {
            templateUrl: "/views/pages/assignation.html",
            controllerAs: "assignation",
            resolve: {
                user: function (UsersSvc) { return UsersSvc.checkConnected(); },
                visiteurs: function (VisiteursSvc) { return VisiteursSvc.promise; },
                secteurs: function (SecteursSvc) { return SecteursSvc.promise; }
            },
            controller: function (VisiteursSvc, SecteursSvc) {
                var self = this;
                self.visiteurs = VisiteursSvc.visiteurs;
                self.secteurs = SecteursSvc.secteurs;
                self.visiteur = null;
                self.regionSecteur = null;

                self.secteursPossibles = function (secteur) {
                    return self.visiteur == null || self.visiteur.Secteurs.filter(function (item) {
                        return item.Id == secteur.Id
                    }).length == 0;
                };

                self.secteursAssignes = function (secteur) {
                    return self.visiteur == null || self.visiteur.Secteurs.filter(function (item) {
                        return item.Id == secteur.Id
                    }).length > 0;
                };

                self.ajoute = function () {
                    if (self.visiteur && self.regionSecteur) {
                        var assignation = {
                            secteurId: self.regionSecteur.Id,
                            visiteurId: self.visiteur.Id
                        }
                        SecteursSvc.insert(assignation);
                        self.visiteur.Secteurs.push(self.regionSecteur);
                        self.regionSecteur = null;
                    }
                    else
                        alert("Merci de sélectionner un couple Région/Secteur.");
                }

                self.supprime = function (idSecteur) {
                    var assignation = {
                        secteurId: idSecteur,
                        visiteurId: self.visiteur.Id
                    }
                    SecteursSvc.delete(assignation);
                    self.visiteur.Secteurs.splice(self.visiteur.Secteurs.indexOf(idSecteur), 1);
                }
            }
        })

        /* ===== ANGULARJS : ROUTER - DEFAULT ===== */
        .otherwise({ redirectTo: "/" });
});
/*===================================================================================*/
/*	ANGULARJS : SERVICE - VISITEURS
/*===================================================================================*/

app.service("VisiteursSvc", function ($http) {
    var self = this;
    self.visiteurs = [];

    self.promise = $http.get("/api/visiteur.php").success(function (response) {
        self.visiteurs = response;
    });

    self.get = function (idVisiteur) {
        for (var i = 0; i < self.visiteurs.length; i++) {
            if (self.visiteurs[i].Id == idVisiteur)
                return self.visiteurs[i];
        }

        return null;
    };
});

/*===================================================================================*/
/*	ANGULARJS : SERVICE - MEDOCS
/*===================================================================================*/

app.service("MedocsSvc", function ($http) {
    var self = this;
    self.medocs = [];

    self.promise = $http.get("/api/medicament.php").success(function (response) {
        self.medocs = response;
    });

    self.update = function (medoc) {
        $http.post("/api/medicament.php", medoc).success(function (response) {
            if (!response)
                alert("Erreur lors de la mise à jour du prix.");
        });
    }

    self.get = function (idMedoc) {
        for (var i = 0; i < self.medocs.length; i++) {
            if (self.medocs[i].Id == idMedoc)
                return self.medocs[i];
        }

        return null;
    };
});

/*===================================================================================*/
/*	ANGULARJS : SERVICE - SECTEURS
/*===================================================================================*/

app.service("SecteursSvc", function ($http) {
    var self = this;
    self.secteurs = [];

    self.promise = $http.get("/api/secteur.php").success(function (response) {
        self.secteurs = response;
    });

    self.insert = function (assignation) {
        $http.put("/api/secteur.php", assignation).success(function (response) {
            if (!response)
                alert("Erreur lors de l'assignation.");
        });
    }

    self.delete = function (assignation) {
        $http.post("/api/secteur.php", assignation).success(function (response) {
            if (!response)
                alert("Erreur lors de la suppression.");
        });
    }
});

/*===================================================================================*/
/*	ANGULARJS : SERVICE - PRATICIENS
/*===================================================================================*/

app.service('PraticiensSvc', function ($http) {
    var self = this;
    self.praticiens = [];

    self.promise = $http.get("/api/praticien.php").success(function (response) {
        self.praticiens = response;
    });

    self.get = function (idPraticien) {
        for (var i = 0; i < self.praticiens.length; i++) {
            if (self.praticiens[i].Id == idPraticien)
                return self.praticiens[i];
        }

        return null;
    };
});


/*===================================================================================*/
/*	ANGULARJS : SERVICE - USER
/*===================================================================================*/

app.service("UsersSvc", function ($http, $location) {
    var self = this;
    self.user = JSON.parse(window.localStorage.getItem("user"));

    self.deconnexion = function () {
        window.localStorage.removeItem("user");
        self.user = null;
        $location.url("/login");
    }

    self.checkConnected = function () {
        if (!self.user || self.user == null) {
            $location.url("/login");
            return false;
        } else {
            return self.user;
        }
    }
});


/*===================================================================================*/
/*	ANGULARJS : SERVICE - COMMANDES
/*===================================================================================*/

app.service("CommandesSvc", function () {
    var self = this;

    self.get = function (praticien, idCommande) {
        for (var i = 0; i < praticien.Commandes.length; i++) {
            if (praticien.Commandes[i].Id == idCommande)
                return praticien.Commandes[i];
        }

        return null;
    };
});

/*===================================================================================*/
/*	ANGULARJS : CONTROLLER - MENU
/*===================================================================================*/

app.controller("MenuCtrl", function (UsersSvc, $location) {
    var self = this;
    self.user = UsersSvc;

    self.deconnexion = function () {
        self.user = null;
        UsersSvc.deconnexion();
    }

    self.getActive = function (path) {
        return ($location.path().substr(0, path.length) === path) ? "active" : "";
    }
});