<?php
class Secteur
{
    public static function getByVisiteur($visiteurId) {
        $db = new PDO(CONNECTION_STRING, CONNECTION_USER, CONNECTION_PASSWORD);
        $request = $db->prepare("CALL sp_Visiteur_Secteur_Select(?)");
        $request->bindParam(1, $visiteurId);
        $request->execute();
        $secteurs = $request->fetchAll(PDO::FETCH_OBJ);

        foreach($secteurs as $secteur) {
            $secteur->Region = new stdClass();
            $secteur->Region->Id = $secteur->IdRegion;
            $secteur->Region->Nom = $secteur->NomRegion;

            unset($secteur->IdRegion);
            unset($secteur->NomRegion);
        }

        return $secteurs;
    }

    public static function get() {
        $db = new PDO(CONNECTION_STRING, CONNECTION_USER, CONNECTION_PASSWORD);
        $request = $db->prepare("CALL sp_SecteurRegion_Select()");
        $request->execute();
        $secteurs = $request->fetchAll(PDO::FETCH_OBJ);

        foreach($secteurs as $secteur) {
            $secteur->Region = new stdClass();
            $secteur->Region->Id = $secteur->IdRegion;
            $secteur->Region->Nom = $secteur->NomRegion;

            unset($secteur->IdRegion);
            unset($secteur->NomRegion);
        }

        return $secteurs;
    }

    public static function insert($assignation) {
        $db = new PDO(CONNECTION_STRING, CONNECTION_USER, CONNECTION_PASSWORD);
        $request = $db->prepare("CALL sp_SecteurRegion_Insert(?, ?)");
        $request->bindParam(1, $assignation->secteurId);
        $request->bindParam(2, $assignation->visiteurId);
        $request->execute();
        $rows = $request->rowCount();
        return $rows;
    }

    public static function delete($assignation) {
        $db = new PDO(CONNECTION_STRING, CONNECTION_USER, CONNECTION_PASSWORD);
        $request = $db->prepare("CALL sp_SecteurRegion_Delete(?, ?)");
        $request->bindParam(1, $assignation->secteurId);
        $request->bindParam(2, $assignation->visiteurId);
        $request->execute();
        $rows = $request->rowCount();
        return $rows;
    }
}