<?php
set_time_limit(120);
include_once("commandeDetail.class.php");
include_once(__DIR__."/../plugins/mpdf60/mpdf.php");
include_once(__DIR__."/../plugins/PHPMailer/class.phpmailer.php");

class Commande
{
    public static function get($idClient) {
        $db = new PDO(CONNECTION_STRING, CONNECTION_USER, CONNECTION_PASSWORD);
        $request = $db->prepare("CALL sp_Commande_Select(?)");
        $request->bindParam(1, $idClient);
        $request->execute();
        $commandes = $request->fetchAll(PDO::FETCH_OBJ);

        $emptyArray = [];

        if (!$commandes)
            return $emptyArray;

        foreach($commandes as $commande) {
            $commande->Date = date( 'd/m/Y à H:i', strtotime($commande->Date));
            $commande->TotalFacture = 0.00;
            $commande->Details = CommandeDetails::get($commande->Id);

            foreach($commande->Details as $details) {
                $commande->TotalFacture += $details->PrixTotal;
                $commande->TotalRemboursement += $details->Medicament->Prix * $details->Medicament->Categorie->TauxRemboursement;
            }
        }

        return $commandes;
    }

    public static function getById($idCommande) {
        $db = new PDO(CONNECTION_STRING, CONNECTION_USER, CONNECTION_PASSWORD);
        $request = $db->prepare("CALL sp_Commande_SelectById(?)");
        $request->bindParam(1, $idCommande);
        $request->execute();
        $commande = $request->fetch(PDO::FETCH_OBJ);

        $commande->Date = date( 'd/m/Y à H:i', strtotime($commande->Date));
        $commande->TotalFacture = 0.00;
        $commande->Details = CommandeDetails::get($commande->Id);

        foreach($commande->Details as $details)
            $commande->TotalFacture += $details->PrixTotal;

        return $commande;
    }

    public static function insert($commande) {
        $commande->Date = date('Y-m-d H:i:s');
        $db = new PDO(CONNECTION_STRING, CONNECTION_USER, CONNECTION_PASSWORD);
        $request = $db->prepare("CALL sp_Commande_Insert(?, ?, ?, ?)");
        $request->bindParam(1, $commande->Date);
        $request->bindParam(2, $commande->Signature);
        $request->bindParam(3, $commande->Visiteur->Id);
        $request->bindParam(4, $commande->Praticien->Id);
        $request->execute();
        $result = $request->fetch();
        $commande->Id = $result[0];

        foreach($commande->Details as $commandeDetails)
            $commandeDetails->Id = CommandeDetails::insert($commande, $commandeDetails);

        self::_envoiFacture($commande);

        return self::getById($commande->Id);
    }

    private static function _envoiFacture($commande) {

        $lienFacture = self::_genereFacture($commande);

        $message = "Bonjour " . $commande->Praticien->Prenom . " " . $commande->Praticien->Nom . ",<br/><br/>";
        $message .= "Merci pour votre commande du ".date('d/m/Y', strtotime($commande->Date)).", votre facture est disponible en pièce jointe à cet email.<br/><br/>";
        $message .= "Pour annuler cette commande, <strong>contactez rapidement</strong> le commercial dont les coordonnées sont indiquées en haut à gauche de votre facture.<br/><br/>";
        $message .= "<i>Les laboratoires GSB</i>";

        $email = new PHPMailer();
        $email->CharSet   = 'UTF-8';
        $email->IsHTML(true);
        $email->From      = 'konrad.kevin78@gmail.com';
        $email->FromName  = 'Laboratoires GSB';
        $email->Subject   = 'Votre facture pour la commande ' . $commande->Id;
        $email->Body      = $message;
        $email->AddAddress($commande->Praticien->Email);

        $email->AddAttachment($lienFacture, end((explode('/', $lienFacture))));

        return $email->Send();
    }

    private static function _genereFacture($commande) {

        $lienFichier = __DIR__.'/../../public/factures/facture_'.$commande->Id.'.pdf';

        $mpdf = new mPDF('', '', 0, '', 5, 5, 5, 5, 9, 9);
        $mpdf->useSubstitutions = false;
        $mpdf->simpleTables = true;

        $commande->prixTotal = 0;
        $listeMedocs = '';

        foreach ($commande->Details as $detailCommande) {
            $totalDetail = ($detailCommande->Medicament->Prix * $detailCommande->Quantite);
            $listeMedocs .= '<tr>';
            $listeMedocs .= '<td>'.$detailCommande->Medicament->Id.'</td>';
            $listeMedocs .= '<td>'.$detailCommande->Medicament->Nom.'</td>';
            $listeMedocs .= '<td>'.$detailCommande->Medicament->Prix.'</td>';
            $listeMedocs .= '<td>'.$detailCommande->Quantite.'</td>';
            $listeMedocs .= '<td>'.$totalDetail.'</td>';
            $listeMedocs .= '</tr>';
            $commande->prixTotal += $totalDetail;
        }

        // Style
        $htmlPdf = '
        <!DOCTYPE html>
        <html>

        <head>
            <meta charset="UTF-8"/>
            <title>Facture n°' . $commande->Id . '</title>

            <style>
                .gras { font-weight: bold; }
                .text-left { text-align: left; }
                .text-right { text-align: right; }
                .text-center { text-align: center; }
                .infosFacture { border: 1px solid black; width: 70%; padding: 10px; }
                .infosPersonne { border: 1px solid black; width: 70%; padding: 10px; }
                .listeMedocs { width: 100%; border: 1px solid black; border-collapse: collapse; }
                .listeMedocs th { padding: 10px; }
                .listeMedocs thead th, .listeMedocs tfoot td { background: lightgrey; }
                .listeMedocs, .listeMedocs th { border: 1px solid black; text-align: center; vertical-align: middle; }
                .listeMedocs tfoot td { border: 1px solid black; vertical-align: middle; }
                .listeMedocs td { border-right: 1px solid black; padding: 5px; text-align: center; vertical-align: middle; }
            </style>
        </head>

        <body>

        <table style="width:100%;">
            <tr>
                <td style="width:50%;">
                    <img alt="Logo GSB" src="../../public/images/gsb_trans.png" height="200" />
                </td>
                <td style="width:50%; text-align:right;">
                    <table class="infosFacture">
                        <tr>
                            <td style="width:70%; text-align:left;">N° de facture :</td>
                            <td style="width:30%; font-weight:bold; text-align:left;">'.$commande->Id.'</td>
                        </tr>
                        <tr>
                            <td style="text-align:left;">Date de facture :</td>
                            <td style="font-weight:bold; text-align:left;">'.date('d/m/Y', strtotime($commande->Date)).'</td>
                        </tr>
                        <tr>
                            <td style="text-align:left;">N° de client :</td>
                            <td style="font-weight:bold; text-align:left;">'.$commande->Praticien->Id.'</td>
                        </tr>
                        <tr>
                            <td style="text-align:left;">N° du commercial :</td>
                            <td style="font-weight:bold; text-align:left;">'.$commande->Visiteur->Id.'</td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr style="height:50px;"><td colspan="2">&nbsp;</td></tr>

            <tr>
		        <td style="width:50%; text-align:left;">
                    <table class="infosPersonne">
                        <tr>
                            <td style="font-weight:bold; text-align:left;">Informations du commercial :</td>
                        </tr>
                        <tr>
                            <td style="text-align:left;">'.$commande->Visiteur->Prenom.' '.$commande->Visiteur->Nom.'</td>
                        </tr>
                        <tr>
                            <td style="text-align:left;">'.$commande->Visiteur->Telephone.'</td>
                        </tr>
                        <tr>
                            <td style="text-align:left;">'.$commande->Visiteur->Email.'</td>
                        </tr>
                    </table>
		        </td>

		        <td style="width:50%; text-align:right;">
                    <table class="infosPersonne">
                        <tr>
                            <td style="font-weight:bold; text-align:left;">Informations du client :</td>
                        </tr>
                        <tr>
                            <td style="text-align:left;">'.$commande->Praticien->Prenom.' '.$commande->Praticien->Nom.'</td>
                        </tr>
                        <tr>
                            <td style="text-align:left;">'.$commande->Praticien->Adresse.'</td>
                        </tr>
                        <tr>
                            <td style="text-align:left;">'.$commande->Praticien->CodePostal.' '.$commande->Praticien->Ville.'</td>
                        </tr>
                    </table>
		        </td>
            </tr>

            <tr style="height:50px;"><td colspan="2">&nbsp;</td></tr>

            <tr>
		        <td colspan="2">
                    <table class="listeMedocs">
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Nom</th>
                                <th>Prix unitaire</th>
                                <th>Quantité</th>
                                <th>Prix total</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <td colspan="4" style="font-weight:bold; text-align:right;">Total :</td>
                                <td style="text-align:center;">'.$commande->prixTotal.'</td>
                            </tr>
                        </tfoot>
                        <tbody> ' . $listeMedocs . ' </tbody>
                    </table>
		        </td>
            </tr>

        </table>

        </body>

        </html>
        ';

        // Génération du PDF
        $mpdf->WriteHTML($htmlPdf);

        $mpdf->Output($lienFichier, 'F');

        return $lienFichier;
    }
}