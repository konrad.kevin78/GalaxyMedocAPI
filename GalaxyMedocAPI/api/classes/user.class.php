<?php
require_once(__DIR__."/../plugins/PHPMailer/class.phpmailer.php");

class User
{
    public static function get($user) {
        $db = new PDO(CONNECTION_STRING, CONNECTION_USER, CONNECTION_PASSWORD);
        $request = $db->prepare("CALL sp_User_Select(?, ?)");
        $request->bindParam(1, $user->Login);
        $request->bindParam(2, $user->Password);
        $request->execute();
        $userDB = $request->fetch(PDO::FETCH_OBJ);
        return $userDB;
    }

    public static function getNoPassword($user) {
        $db = new PDO(CONNECTION_STRING, CONNECTION_USER, CONNECTION_PASSWORD);
        $request = $db->prepare("CALL sp_User_SelectNoPassword(?)");
        $request->bindParam(1, $user->Login);
        $request->execute();
        $userDB = $request->fetch(PDO::FETCH_OBJ);
        return $userDB;
    }

    public static function update($user) {
        $db = new PDO(CONNECTION_STRING, CONNECTION_USER, CONNECTION_PASSWORD);
        $request = $db->prepare("CALL sp_User_Update(?, ?)");
        $request->bindParam(1, $user->Login);
        $request->bindParam(2, $user->Password);
        $request->execute();
        $rows = $request->rowCount();
        return $rows;
    }

    // TODO : Ne fonctionne pas, il faut permettre de faire un User::get sans le mdp
    public static function recover($user) {
        $user = User::getNoPassword($user);

        if (!$user)
            return false;

        $user->Password = User::randomPassword(7);

        if(!User::update($user))
            return false;

        $message = "Bonjour " . $user->Prenom . " " . $user->Nom . ",<br/><br/>";
        $message .= "Dans le cadre de votre demande de récupération de mot de passe depuis l'application mobile GalaxyMedoc, nous vous informons de vos nouveaux identifiants de connexion à cette dernière :<br/><br/>";
        $message .= "- Nom d'utilisateur : <b>" . $user->Login . "</b><br/>";
        $message .= "- Mot de passe : <b>" . $user->Password . "</b><br/><br/>";
        $message .= "À bientôt sur notre application mobile !<br/><br/>";
        $message .= "L'équipe GalaxyMedoc";

        $email = new PHPMailer();
        $email->CharSet   = 'UTF-8';
        $email->IsHTML(true);
        $email->From      = 'konrad.kevin78@gmail.com';
        $email->FromName  = 'L\'équipe GalaxyMedoc';
        $email->Subject   = 'GalaxyMedoc - Récupération de mot de passe';
        $email->Body      = $message;
        $email->AddAddress($user->Email);

        return $email->Send();
    }

    public static function randomPassword($length) {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;

        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }

        return implode($pass);
    }
}