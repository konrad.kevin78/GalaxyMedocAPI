<?php
include_once("commande.class.php");

class Praticien
{
    public static function get() {
        $db = new PDO(CONNECTION_STRING, CONNECTION_USER, CONNECTION_PASSWORD);
        $request = $db->prepare("CALL sp_Praticien_Select()");
        $request->execute();
        $praticiens = $request->fetchAll(PDO::FETCH_OBJ);

        foreach ($praticiens as $praticien) {
            $praticien->Secteur = new stdClass();
            $praticien->Secteur->Id = $praticien->secteurId;
            $praticien->Secteur->Nom = $praticien->secteurNom;
            $praticien->Secteur->Region = new stdClass();
            $praticien->Secteur->Region->Id = $praticien->regionId;
            $praticien->Secteur->Region->Nom = $praticien->regionNom;
            $praticien->TotalPaye = 0.00;
            $praticien->Commandes = Commande::get($praticien->Id);

            foreach($praticien->Commandes as $commande)
                $praticien->TotalPaye += $commande->TotalFacture;

            unset($praticien->secteurId);
            unset($praticien->secteurNom);
            unset($praticien->regionId);
            unset($praticien->regionNom);
        }

        return $praticiens;
    }
}