<?php
require_once("secteur.class.php");

class Visiteur
{
    public static function get() {
        $db = new PDO(CONNECTION_STRING, CONNECTION_USER, CONNECTION_PASSWORD);
        $request = $db->prepare("CALL sp_Visiteur_Select()");
        $request->execute();
        $visiteurs = $request->fetchAll(PDO::FETCH_OBJ);

        foreach ($visiteurs as $visiteur)
            $visiteur->Secteurs = Secteur::getByVisiteur($visiteur->Id);

        return $visiteurs;
    }
}