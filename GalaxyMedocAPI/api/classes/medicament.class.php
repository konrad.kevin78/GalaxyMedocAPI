<?php

class Medicament
{
    public static function get() {
        $db = new PDO(CONNECTION_STRING, CONNECTION_USER, CONNECTION_PASSWORD);
        $request = $db->prepare("CALL sp_Medicament_Select");
        $request->execute();
        $medicaments = $request->fetchAll(PDO::FETCH_OBJ);
        return $medicaments;
    }

    public static function update($medicament) {
        $db = new PDO(CONNECTION_STRING, CONNECTION_USER, CONNECTION_PASSWORD);
        $request = $db->prepare("CALL sp_Medicament_Update(?, ?, ?, ?, ?, ?)");
        $request->bindParam(1, $medicament->Id);
        $request->bindParam(2, $medicament->Nom);
        $request->bindParam(3, $medicament->Composition);
        $request->bindParam(4, $medicament->Effets);
        $request->bindParam(5, $medicament->ContreIndications);
        $request->bindParam(6, $medicament->Prix);
        $request->execute();
        $rows = $request->rowCount();
        return $rows;
    }

    public static function insert($medicament) {
        $db = new PDO(CONNECTION_STRING, CONNECTION_USER, CONNECTION_PASSWORD);
        $request = $db->prepare("CALL sp_Medicament_Insert(?, ?, ?, ?, ?, ?)");
        $request->bindParam(1, $medicament->Id);
        $request->bindParam(2, $medicament->Nom);
        $request->bindParam(3, $medicament->Composition);
        $request->bindParam(4, $medicament->Effets);
        $request->bindParam(5, $medicament->ContreIndications);
        $request->bindParam(6, $medicament->Prix);
        $request->execute();
        $rows = $request->rowCount();
        return $rows;
    }
}