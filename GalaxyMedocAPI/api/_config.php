<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, DELETE, PUT");
header("Access-Control-Allow-Credentials: true");

const CONNECTION_STRING = "mysql:host=MYSQL_HOST;dbname=MYSQL_DATABASE;charset=UTF8";
const CONNECTION_USER = "MYSQL_USER";
const CONNECTION_PASSWORD = "MYSQL_PASSWORD";

$post = file_get_contents("php://input");
$data = json_decode($post);
$response = false;